package com.hrd._Mann_Socheat_Spring_Homework002.controller;


import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Invoice;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.InvoiceRequest;
import com.hrd._Mann_Socheat_Spring_Homework002.model.respone.InvoiceResponse;
import com.hrd._Mann_Socheat_Spring_Homework002.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invoice")
public class InvoiceController {
   private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("all")
    @Operation(summary = "Get all invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .payload(invoiceService.getAllInvoice())
                .message("Get Invoice successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(response);
    }
    @GetMapping("/SearchById{invoice_id}")
    @Operation(summary = "Search by Id")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceInvoiceById(@PathVariable("invoice_id") Integer InvoiceId){
        InvoiceResponse<Invoice> response=null;
        if (invoiceService.getInvoiceById(InvoiceId) !=null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(InvoiceId))
                    .message("Search Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("Invoice not exit")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }

    @DeleteMapping("/deleteInvoice{id}")
    @Operation(summary = "Delete invoice by id")
    public ResponseEntity<InvoiceResponse<String>> deleteProductById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<String> response = null;
        if (invoiceService.deleteInvoiceById(invoiceId)){
            response = InvoiceResponse.<String>builder()
                    .message("Delete Invoice successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping("/")
    @Operation(summary = "Insert invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){

        InvoiceResponse<Invoice> response = null;
        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);
        if (storeInvoiceId !=null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Add new invoice successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("No Data")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PutMapping("/update-invoice-by-id{id}")
    @Operation(summary = "Update Invoice By Id")
    public ResponseEntity<InvoiceResponse<Invoice>>  updateInvoice(@RequestBody InvoiceRequest invoiceRequest, @PathVariable("id") Integer id){
        InvoiceResponse<Invoice> response = null;
        Integer storeInvoiceId = invoiceService.updateInvoice(invoiceRequest,id);
        if(storeInvoiceId != null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Update Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
