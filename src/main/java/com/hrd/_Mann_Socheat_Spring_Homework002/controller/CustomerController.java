package com.hrd._Mann_Socheat_Spring_Homework002.controller;


import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Customer;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.CustomerRequest;
import com.hrd._Mann_Socheat_Spring_Homework002.model.respone.CustomerRespone;
import com.hrd._Mann_Socheat_Spring_Homework002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all customer")
    public ResponseEntity<CustomerRespone <List<Customer>>>  getAllCustomers(){
        CustomerRespone<List<Customer>> response = CustomerRespone.<List<Customer>>builder()
                .message("Get customer successfully")
                .payload(customerService.getAllCustomer())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getById{customer_id}")
    @Operation(summary = "Search by id")
    public ResponseEntity<CustomerRespone<Customer>> searchById(@PathVariable("customer_id") Integer customerId){
        CustomerRespone<Customer> response= null;
        if (customerService.getCustomerById(customerId) !=null){
             response = CustomerRespone.<Customer>builder()
                    .message("Search successfully ")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
             return ResponseEntity.ok(response);
        }else {
            response = CustomerRespone.<Customer>builder()
                    .message("Customer not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }}
    @DeleteMapping("/{customerId}")
    @Operation(summary = "Delete by Id")
    public ResponseEntity<CustomerRespone<String>> deleteCustomerById(@PathVariable Integer customerId){
        CustomerRespone<String> response=null;
        if (customerService.deleteCustomerById(customerId) == true){
             response = CustomerRespone.<String>builder()
                    .message("Delete customer successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/")
    @Operation(summary = "Insert customer")
    public ResponseEntity<CustomerRespone<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);
        if ( storeCustomerId !=null){
            CustomerRespone<Customer> response = CustomerRespone.<Customer>builder()
                    .message("Insert Successfully")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return ResponseEntity.ok(response);
        }

        return null;
    }

    @PutMapping("/{customerId}")
    @Operation(summary = "Update by id")
    public ResponseEntity<CustomerRespone<Customer>> updateCustomerById(@RequestBody CustomerRequest customerRequest, @PathVariable("customerId") Integer customerId){
        Integer updateById = customerService.updateCustomer(customerRequest,customerId);
        CustomerRespone<Customer> response =null;
        if (updateById !=null){
            response = CustomerRespone.<Customer>builder()
                    .message("Update successfully")
                    .payload(customerService.getCustomerById(updateById))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = CustomerRespone.<Customer>builder()
                    .message("Data not Found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}
