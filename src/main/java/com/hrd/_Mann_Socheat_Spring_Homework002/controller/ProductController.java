package com.hrd._Mann_Socheat_Spring_Homework002.controller;


import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Product;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.ProductRequest;
import com.hrd._Mann_Socheat_Spring_Homework002.model.respone.ProductResponse;
import com.hrd._Mann_Socheat_Spring_Homework002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all Product")
public ResponseEntity<ProductResponse<List<Product>>> getAllProducts(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProduct())
                .message("Get Product Successfully")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
}

@GetMapping("findProduct{product_id}")
@Operation(summary = "Find Product by Id")
public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("product_id") Integer productId){
        ProductResponse<Product> response =null;
        if (productService.getProductById(productId) !=null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("Find Product Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response=ProductResponse.<Product>builder()
                    .message("Product Not Fond")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
}

@PostMapping("/")
@Operation(summary = "Insert Product")
public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){

        Integer storeProduct = productService.addNewProduct(productRequest);
        if (storeProduct !=null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(storeProduct))
                    .message("Insert Product Successfully")
                    .success(true)
                    .build();

            return ResponseEntity.ok(response);
        }
return null;
}

@PutMapping("/update{product_id}")
public ResponseEntity<ProductResponse<Product>> getUpdateProductById(@RequestBody ProductRequest productRequest, @PathVariable("product_id") Integer productId){
        Integer updateById = productService.getUpdateproduct(productRequest,productId);
        ProductResponse<Product> response=null;
        if (updateById !=null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(updateById))
                    .message("Update Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Product>builder()
                    .message("Product Not Found")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
}

@DeleteMapping("/deleteProduct{product_id}")
    @Operation(summary = "Delete product by Id")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("product_id") Integer invoiceId){
        ProductResponse<String> response=null;
        if (productService.deleteProductById(invoiceId)==true){
            response = ProductResponse.<String>builder()
                    .message("Delete product Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }

        return ResponseEntity.notFound().build();

}
}
