package com.hrd._Mann_Socheat_Spring_Homework002.service.serviceImpl;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Customer;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.CustomerRequest;
import com.hrd._Mann_Socheat_Spring_Homework002.repository.CustomerRepository;
import com.hrd._Mann_Socheat_Spring_Homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerServiceImpl implements CustomerService {

private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getFindAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId=customerRepository.saveCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerUpdate = customerRepository.updateCustomer(customerRequest, customerId);
        return customerUpdate;
    }


}
