package com.hrd._Mann_Socheat_Spring_Homework002.service.serviceImpl;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Invoice;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.InvoiceRequest;
import com.hrd._Mann_Socheat_Spring_Homework002.repository.InvoiceRepository;
import com.hrd._Mann_Socheat_Spring_Homework002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getFindAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer InvoiceId) {
        return invoiceRepository.getInvoiceById(InvoiceId);
    }

    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer storeInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);
        for (Integer productId:invoiceRequest.getInvoice()) {
            invoiceRepository.saveProductByInvoice(storeInvoiceId,productId);
        }
        return storeInvoiceId;
    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId) {
        Integer storeInvoiceId = invoiceRepository.updateInvoiceById(invoiceRequest,invoiceId);
        invoiceRepository.delete(invoiceId);
        for(Integer update : invoiceRequest.getInvoice()){
            invoiceRepository.saveProductByInvoice(storeInvoiceId,update);
        }
        return storeInvoiceId;
    }
}
