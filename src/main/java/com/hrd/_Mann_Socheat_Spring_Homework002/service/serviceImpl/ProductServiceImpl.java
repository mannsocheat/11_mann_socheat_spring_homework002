package com.hrd._Mann_Socheat_Spring_Homework002.service.serviceImpl;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Product;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.ProductRequest;
import com.hrd._Mann_Socheat_Spring_Homework002.repository.ProductRepository;
import com.hrd._Mann_Socheat_Spring_Homework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {

        return productRepository.getFindAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getFindProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        return productRepository.saveProduct(productRequest);
    }

    @Override
    public Integer getUpdateproduct(ProductRequest productRequest, Integer productId) {
        return productRepository.updateProduct(productRequest, productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }
}
