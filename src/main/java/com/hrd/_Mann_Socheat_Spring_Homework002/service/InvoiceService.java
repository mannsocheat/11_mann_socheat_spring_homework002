package com.hrd._Mann_Socheat_Spring_Homework002.service;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Invoice;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();
    Invoice getInvoiceById(Integer InvoiceId);
    boolean deleteInvoiceById(Integer invoiceId);
    Integer addNewInvoice(InvoiceRequest invoiceRequest);
    Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId);
}
