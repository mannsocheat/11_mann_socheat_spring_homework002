package com.hrd._Mann_Socheat_Spring_Homework002.service;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Product;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    Product getProductById(Integer productId);

    Integer addNewProduct(ProductRequest productRequest);

    Integer getUpdateproduct(ProductRequest productRequest, Integer productId);
    boolean deleteProductById(Integer productId);

}
