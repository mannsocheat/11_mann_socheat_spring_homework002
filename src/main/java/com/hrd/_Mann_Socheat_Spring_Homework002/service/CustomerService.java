package com.hrd._Mann_Socheat_Spring_Homework002.service;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Customer;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.CustomerRequest;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomer();
    Customer getCustomerById(Integer customerId);

    boolean deleteCustomerById(Integer customerId);

    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);

}
