package com.hrd._Mann_Socheat_Spring_Homework002.repository;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Invoice;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoice_tb")
    @Results(
            id = "Mapping",
            value = {
                    @Result(property = "invoice_id", column = "invoice_id"),
                    @Result(property = "customer", column = "customer_id",
                            one = @One(select="com.hrd._Mann_Socheat_Spring_Homework002.repository.CustomerRepository.getCustomerById")),
                    @Result(property = "products", column = "invoice_id",
                            many  =@Many(select = "com.hrd._Mann_Socheat_Spring_Homework002.repository.ProductRepository.getAllProduct"))
            }
    )
    List<Invoice> getFindAllInvoice();

    @Select("SELECT * FROM invoice_tb  WHERE invoice_id=#{InvoiceId} ")
    @ResultMap("Mapping")
    Invoice getInvoiceById(Integer InvoiceId);

    @Delete("DELETE FROM invoice_tb WHERE invoice_id=#{invoiceId} ")
    boolean deleteInvoiceById(Integer invoiceId);

    @Select("INSERT INTO invoice_tb (invoice_date, customer_id) " +
            "VALUES(#{request.invoice_date}, #{request.customerId}) "+
    "RETURNING invoice_id")
    Integer saveInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Select("INSERT INTO invoice_detail_tb (invoice_id, product_id)" +
            "VALUES(#{invoice_id}, #{product_id})")
    Integer saveProductByInvoice(Integer invoice_id, Integer product_id);

    @Select("UPDATE invoice_tb " +
            "SET invoice_date=#{request.invoice_date}, " +
            "customer_id=#{request.customerId} " +
            "WHERE invoice_id=#{invoice_id} " +
            "RETURNING invoice_id")
    Integer updateInvoiceById(@Param("request") InvoiceRequest invoiceRequest, Integer invoice_id);

    @Delete("DELETE FROM invoice_detail_tb WHERE invoice_id=#{invoice_id} ")
    boolean delete(Integer invoiceId);
}
