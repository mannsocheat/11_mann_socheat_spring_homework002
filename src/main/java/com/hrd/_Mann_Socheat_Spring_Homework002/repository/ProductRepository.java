package com.hrd._Mann_Socheat_Spring_Homework002.repository;

import com.hrd._Mann_Socheat_Spring_Homework002.model.entity.Product;
import com.hrd._Mann_Socheat_Spring_Homework002.model.reguest.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT p.product_id, p.product_name,p.product_price FROM invoice_detail_tb ib "+
            "INNER JOIN product_tb p ON p.product_id = ib.product_id "+
            "WHERE ib.invoice_id=#{invoiceId}; ")
    List<Product> getAllProduct(Integer invoiceId);

    @Select("SELECT * FROM product_tb ORDER BY product_id")
    List<Product> getFindAllProduct();

    @Select("SELECT * FROM product_tb WHERE product_id=#{productId}")
    Product getFindProductById(Integer productId);

    @Select("INSERT INTO product_tb (product_name, product_price) VALUES(#{request.product_name},#{request.product_price}) "+"RETURNING product_id ")
    Integer saveProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE product_tb "+
            "SET product_name=#{request.product_name}, " +
            "product_price=#{request.product_price} " +
            "WHERE product_id=#{productId} " +
            "RETURNING product_id ")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);

    @Delete("DELETE FROM product_tb WHERE product_id = #{productId} ")
    boolean deleteProductById(Integer productId);

}
