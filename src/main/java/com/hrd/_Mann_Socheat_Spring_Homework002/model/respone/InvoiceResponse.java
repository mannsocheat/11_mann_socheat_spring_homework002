package com.hrd._Mann_Socheat_Spring_Homework002.model.respone;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceResponse <T>{

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private boolean success;

}
