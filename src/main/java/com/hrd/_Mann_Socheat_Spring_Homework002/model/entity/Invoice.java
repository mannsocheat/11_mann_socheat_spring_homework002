package com.hrd._Mann_Socheat_Spring_Homework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Invoice {
    private Integer invoice_id;
   private Timestamp invoice_date;
    private Customer customer;
    private List<Product> products = new ArrayList<>();
}
