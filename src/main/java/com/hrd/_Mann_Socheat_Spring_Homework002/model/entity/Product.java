package com.hrd._Mann_Socheat_Spring_Homework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    private Integer product_id;
    private String product_name;
    private float product_price;
}
