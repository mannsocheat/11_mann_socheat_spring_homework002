package com.hrd._Mann_Socheat_Spring_Homework002.model.entity;


import lombok.*;

@Setter
@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    private Integer customer_id;
    private String customer_name;
    private String customer_address;
    private String customer_phone;
}
