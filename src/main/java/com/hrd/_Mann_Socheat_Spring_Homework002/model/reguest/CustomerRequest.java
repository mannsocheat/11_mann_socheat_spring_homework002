package com.hrd._Mann_Socheat_Spring_Homework002.model.reguest;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRequest {
    private String customer_name;
    private String customer_address;
    private String customer_phone;

}
