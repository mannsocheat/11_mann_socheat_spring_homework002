package com.hrd._Mann_Socheat_Spring_Homework002.model.reguest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {
    private Timestamp invoice_date;
    private Integer customerId;
    private List<Integer> invoice;
}
