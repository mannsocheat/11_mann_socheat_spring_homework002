package com.hrd._Mann_Socheat_Spring_Homework002.model.reguest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductRequest {
    private String product_name;
    private float product_price;
}
